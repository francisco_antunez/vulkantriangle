#VULKAN_SDK_PATH=/usr/
DEBUG = -ggdb -Wall
CFLAGS = -std=c++17 ${DEBUG} #-I$(VULKAN_SDK_PATH)/include
LDFLAGS = `pkg-config glfw3 --static --libs` -lvulkan

B = build/

testV: main.o validate.o triangleApp.o queueFamilies.o
	g++ $(CFLAGS) -o VulkanTest main.o validate.o triangleApp.o queueFamilies.o $(LDFLAGS)

main.o: main.cpp
	g++ $(CFLAGS) -c main.cpp 

triangleApp.o: triangleApp.h validate.h queueFamilies.h triangleApp.cpp 
	g++ $(CFLAGS) -c triangleApp.cpp 

validate.o: validate.h validate.cpp
	g++ $(CFLAGS) -c validate.cpp

queueFamilies.o: queueFamilies.h queueFamilies.cpp
	g++ $(CFLAGS) -c queueFamilies.cpp

.PHONY: test clean

test: VulkanTest
	./VulkanTest

debugtest: VulkanTest
	VK_LAYER_PATH=/etc/vulkan/explicit_layer.d ./VulkanTest

clean:
	rm -f VulkanTest
	rm *.o