#ifndef TRIANGLEAPP_H
#define TRIANGLEAPP_H

#include "validate.h"
#include "queueFamilies.h"

const uint32_t WIDTH = 800;
const uint32_t HEIGHT = 600;


class HelloTriangleApplication : public Validate {
    public:
        void run();
    
    private:
        GLFWwindow* window;
        VkInstance instance;
        VkDevice device;
        // I don't know what's happening here, but VK_NULL_HANDLE make it crash
        VkPhysicalDevice physicalDevice;// = VK_NULL_HANDLE;
        VkQueue graphicsQueue;

        void createInstance();
        void initWindow();
        void pickPhysicalDevice();
        void initVulkan();
        void mainLoop();
        void cleanup();
        void createLogicalDevice();
};

#endif