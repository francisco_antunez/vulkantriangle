#include "queueFamilies.h"

QueueFamilyIndices Qf::findQueueFamilies(VkPhysicalDevice device) {

    QueueFamilyIndices indices;
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

    int i = 0;
    for (const auto& queueFamily : queueFamilies) {
        // check for graphics bit in flags
        // set the index when it's found
        if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            indices.graphicsFamily = i;
        }
        // as soon as it's found (graphicsFamily.has_value()) break
        if (indices.isComplete()) {
            break;
        }

        i++;
    }
    // everything that doesn't have the graphics bit will have no value by the time they're here
    return indices;
}

// I'm not sure if this should be public with more code implemented 
// I made it public for Logical device and queues

bool Qf::isDeviceSuitable(VkPhysicalDevice device) {

    QueueFamilyIndices indices = Qf::findQueueFamilies(device);

    // this can be used to select a gpu automatically
    // the tutorial shows a way af doing it with scores
    // there's also the option of asking the user

    // https://vulkan-tutorial.com/en/Drawing_a_triangle/Setup/Physical_devices_and_queue_families

    // VkPhysicalDeviceProperties deviceProperties;
    // VkPhysicalDeviceFeatures deviceFeatures;
    // vkGetPhysicalDeviceProperties(device, &deviceProperties);
    // vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

    // just use any device
    return indices.isComplete();
}