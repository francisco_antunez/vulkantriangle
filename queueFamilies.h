#ifndef QUEUEFAMILIES_H
#define QUEUEFAMILIES_H

#include <vulkan/vulkan.h>
#include <optional>
#include <vector>

struct QueueFamilyIndices {
    std::optional<uint32_t> graphicsFamily;

    bool isComplete() {
        return graphicsFamily.has_value();
    }
};

class Qf {
    public:
        static bool isDeviceSuitable(VkPhysicalDevice device);
        static QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

    private:
        // nothing is private at the moment
};

#endif